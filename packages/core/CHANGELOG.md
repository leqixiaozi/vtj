# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.8.163](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.162...@vtj/core@0.8.163) (2024-10-17)

**Note:** Version bump only for package @vtj/core





## [0.8.162](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.161...@vtj/core@0.8.162) (2024-10-15)

**Note:** Version bump only for package @vtj/core





## [0.8.161](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.160...@vtj/core@0.8.161) (2024-10-14)

**Note:** Version bump only for package @vtj/core






## [0.8.160](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.159...@vtj/core@0.8.160) (2024-10-14)

**Note:** Version bump only for package @vtj/core






## [0.8.159](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.158...@vtj/core@0.8.159) (2024-10-11)

**Note:** Version bump only for package @vtj/core






## [0.8.158](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.157...@vtj/core@0.8.158) (2024-10-11)


### Features

* ✨ 区块管理支持分组 ([08f64a0](https://gitee.com/newgateway/vtj/commits/08f64a09088ef42142fc607ba8099afe6946c35f))
* ✨ api分组 ([bd029b0](https://gitee.com/newgateway/vtj/commits/bd029b0418ef48afac4830ed158e00c1771a55e0))





## [0.8.157](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.156...@vtj/core@0.8.157) (2024-10-09)

**Note:** Version bump only for package @vtj/core





## [0.8.156](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.155...@vtj/core@0.8.156) (2024-10-08)


### Bug Fixes

* 🐛 saveProject remove  page dsl attrs ([b87a28e](https://gitee.com/newgateway/vtj/commits/b87a28e8122282953b0c5e5d4282005bcb6cf730))
* 🐛 XDialog classList bug ([d96b8ba](https://gitee.com/newgateway/vtj/commits/d96b8ba4901f69249707a955638a40902d79bd73))






## [0.8.155](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.154...@vtj/core@0.8.155) (2024-10-07)

**Note:** Version bump only for package @vtj/core






## [0.8.154](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.153...@vtj/core@0.8.154) (2024-10-07)

**Note:** Version bump only for package @vtj/core





## [0.8.153](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.152...@vtj/core@0.8.153) (2024-10-07)

**Note:** Version bump only for package @vtj/core





## [0.8.152](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.151...@vtj/core@0.8.152) (2024-10-04)

**Note:** Version bump only for package @vtj/core





## [0.8.151](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.150...@vtj/core@0.8.151) (2024-10-04)

**Note:** Version bump only for package @vtj/core






## [0.8.150](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.149...@vtj/core@0.8.150) (2024-09-30)

**Note:** Version bump only for package @vtj/core





## [0.8.149](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.148...@vtj/core@0.8.149) (2024-09-27)

**Note:** Version bump only for package @vtj/core





## [0.8.148](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.147...@vtj/core@0.8.148) (2024-09-27)

**Note:** Version bump only for package @vtj/core






## [0.8.147](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.146...@vtj/core@0.8.147) (2024-09-25)


### Bug Fixes

* 🐛 复制节点对象引用问题 ([7789076](https://gitee.com/newgateway/vtj/commits/7789076f31b8c56697ce4affe3eacd2932df1cda))






## [0.8.146](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.145...@vtj/core@0.8.146) (2024-09-20)

**Note:** Version bump only for package @vtj/core






## [0.8.145](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.144...@vtj/core@0.8.145) (2024-09-20)

**Note:** Version bump only for package @vtj/core






## [0.8.144](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.143...@vtj/core@0.8.144) (2024-09-19)

**Note:** Version bump only for package @vtj/core





## [0.8.143](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.142...@vtj/core@0.8.143) (2024-09-19)

**Note:** Version bump only for package @vtj/core





## [0.8.142](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.141...@vtj/core@0.8.142) (2024-09-18)

**Note:** Version bump only for package @vtj/core





## [0.8.141](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.140...@vtj/core@0.8.141) (2024-09-18)

**Note:** Version bump only for package @vtj/core






## [0.8.140](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.139...@vtj/core@0.8.140) (2024-09-18)

**Note:** Version bump only for package @vtj/core





## [0.8.139](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.138...@vtj/core@0.8.139) (2024-09-16)

**Note:** Version bump only for package @vtj/core





## [0.8.138](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.137...@vtj/core@0.8.138) (2024-09-16)

**Note:** Version bump only for package @vtj/core





## [0.8.137](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.136...@vtj/core@0.8.137) (2024-09-16)

**Note:** Version bump only for package @vtj/core






## [0.8.136](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.135...@vtj/core@0.8.136) (2024-09-14)

**Note:** Version bump only for package @vtj/core






## [0.8.135](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.134...@vtj/core@0.8.135) (2024-09-13)

**Note:** Version bump only for package @vtj/core





## [0.8.134](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.133...@vtj/core@0.8.134) (2024-09-12)

**Note:** Version bump only for package @vtj/core






## [0.8.133](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.132...@vtj/core@0.8.133) (2024-09-12)

**Note:** Version bump only for package @vtj/core





## [0.8.132](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.131...@vtj/core@0.8.132) (2024-09-12)

**Note:** Version bump only for package @vtj/core





## [0.8.131](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.130...@vtj/core@0.8.131) (2024-09-12)


### Features

* ✨ devtools module ([18e2949](https://gitee.com/newgateway/vtj/commits/18e294909e0533119f2d6f7d9fa33d470f3a6abb))






## [0.8.130](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.129...@vtj/core@0.8.130) (2024-09-10)

**Note:** Version bump only for package @vtj/core





## [0.8.129](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.128...@vtj/core@0.8.129) (2024-09-10)

**Note:** Version bump only for package @vtj/core






## [0.8.128](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.127...@vtj/core@0.8.128) (2024-09-09)

**Note:** Version bump only for package @vtj/core






## [0.8.127](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.126...@vtj/core@0.8.127) (2024-09-09)

**Note:** Version bump only for package @vtj/core






## [0.8.126](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.125...@vtj/core@0.8.126) (2024-09-09)

**Note:** Version bump only for package @vtj/core






## [0.8.125](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.124...@vtj/core@0.8.125) (2024-09-08)

**Note:** Version bump only for package @vtj/core





## [0.8.124](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.123...@vtj/core@0.8.124) (2024-09-06)

**Note:** Version bump only for package @vtj/core






## [0.8.123](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.122...@vtj/core@0.8.123) (2024-09-02)

**Note:** Version bump only for package @vtj/core





## [0.8.122](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.121...@vtj/core@0.8.122) (2024-09-02)

**Note:** Version bump only for package @vtj/core






## [0.8.121](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.120...@vtj/core@0.8.121) (2024-09-02)

**Note:** Version bump only for package @vtj/core






## [0.8.120](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.119...@vtj/core@0.8.120) (2024-08-31)

**Note:** Version bump only for package @vtj/core






## [0.8.119](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.118...@vtj/core@0.8.119) (2024-08-29)

**Note:** Version bump only for package @vtj/core





## [0.8.118](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.117...@vtj/core@0.8.118) (2024-08-29)

**Note:** Version bump only for package @vtj/core






## [0.8.117](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.116...@vtj/core@0.8.117) (2024-08-29)

**Note:** Version bump only for package @vtj/core





## [0.8.116](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.115...@vtj/core@0.8.116) (2024-08-28)

**Note:** Version bump only for package @vtj/core





## [0.8.115](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.114...@vtj/core@0.8.115) (2024-08-28)

**Note:** Version bump only for package @vtj/core






## [0.8.114](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.113...@vtj/core@0.8.114) (2024-08-27)

**Note:** Version bump only for package @vtj/core






## [0.8.113](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.112...@vtj/core@0.8.113) (2024-08-23)

**Note:** Version bump only for package @vtj/core





## [0.8.112](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.111...@vtj/core@0.8.112) (2024-08-22)

**Note:** Version bump only for package @vtj/core





## [0.8.111](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.110...@vtj/core@0.8.111) (2024-08-22)

**Note:** Version bump only for package @vtj/core





## [0.8.110](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.109...@vtj/core@0.8.110) (2024-08-22)

**Note:** Version bump only for package @vtj/core






## [0.8.109](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.108...@vtj/core@0.8.109) (2024-08-20)

**Note:** Version bump only for package @vtj/core






## [0.8.108](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.107...@vtj/core@0.8.108) (2024-08-18)

**Note:** Version bump only for package @vtj/core





## [0.8.107](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.106...@vtj/core@0.8.107) (2024-08-17)

**Note:** Version bump only for package @vtj/core





## [0.8.106](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.105...@vtj/core@0.8.106) (2024-08-17)

**Note:** Version bump only for package @vtj/core





## [0.8.105](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.104...@vtj/core@0.8.105) (2024-08-16)

**Note:** Version bump only for package @vtj/core





## [0.8.104](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.103...@vtj/core@0.8.104) (2024-08-16)

**Note:** Version bump only for package @vtj/core






## [0.8.103](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.102...@vtj/core@0.8.103) (2024-08-14)

**Note:** Version bump only for package @vtj/core





## [0.8.102](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.101...@vtj/core@0.8.102) (2024-08-13)

**Note:** Version bump only for package @vtj/core





## [0.8.101](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.100...@vtj/core@0.8.101) (2024-08-13)

**Note:** Version bump only for package @vtj/core






## [0.8.100](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.99...@vtj/core@0.8.100) (2024-08-09)

**Note:** Version bump only for package @vtj/core





## [0.8.99](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.98...@vtj/core@0.8.99) (2024-08-08)

**Note:** Version bump only for package @vtj/core





## [0.8.98](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.97...@vtj/core@0.8.98) (2024-08-08)


### Bug Fixes

* 🐛 删除页面或目录 ([fd5aa9b](https://gitee.com/newgateway/vtj/commits/fd5aa9b3fa68b0b708eaed8f0f06faf4ad5091f8)), closes [#IAH862](https://gitee.com/newgateway/vtj/issues/IAH862)






## [0.8.97](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.96...@vtj/core@0.8.97) (2024-07-31)

**Note:** Version bump only for package @vtj/core






## [0.8.96](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.95...@vtj/core@0.8.96) (2024-07-25)

**Note:** Version bump only for package @vtj/core





## [0.8.95](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.94...@vtj/core@0.8.95) (2024-07-22)

**Note:** Version bump only for package @vtj/core





## [0.8.94](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.93...@vtj/core@0.8.94) (2024-07-20)

**Note:** Version bump only for package @vtj/core





## [0.8.93](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.92...@vtj/core@0.8.93) (2024-07-20)

**Note:** Version bump only for package @vtj/core





## [0.8.92](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.91...@vtj/core@0.8.92) (2024-07-19)

**Note:** Version bump only for package @vtj/core





## [0.8.91](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.90...@vtj/core@0.8.91) (2024-07-19)

**Note:** Version bump only for package @vtj/core





## [0.8.90](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.89...@vtj/core@0.8.90) (2024-07-18)

**Note:** Version bump only for package @vtj/core





## [0.8.89](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.88...@vtj/core@0.8.89) (2024-07-18)

**Note:** Version bump only for package @vtj/core





## [0.8.88](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.87...@vtj/core@0.8.88) (2024-07-16)

**Note:** Version bump only for package @vtj/core





## [0.8.87](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.86...@vtj/core@0.8.87) (2024-07-16)

**Note:** Version bump only for package @vtj/core





## [0.8.86](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.85...@vtj/core@0.8.86) (2024-07-15)

**Note:** Version bump only for package @vtj/core





## [0.8.85](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.84...@vtj/core@0.8.85) (2024-07-15)

**Note:** Version bump only for package @vtj/core





## [0.8.84](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.83...@vtj/core@0.8.84) (2024-07-15)

**Note:** Version bump only for package @vtj/core





## [0.8.83](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.82...@vtj/core@0.8.83) (2024-07-12)

**Note:** Version bump only for package @vtj/core





## [0.8.82](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.81...@vtj/core@0.8.82) (2024-07-12)

**Note:** Version bump only for package @vtj/core





## [0.8.81](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.80...@vtj/core@0.8.81) (2024-07-12)

**Note:** Version bump only for package @vtj/core





## [0.8.80](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.79...@vtj/core@0.8.80) (2024-07-10)

**Note:** Version bump only for package @vtj/core





## [0.8.79](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.78...@vtj/core@0.8.79) (2024-07-10)

**Note:** Version bump only for package @vtj/core





## [0.8.78](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.77...@vtj/core@0.8.78) (2024-07-09)

**Note:** Version bump only for package @vtj/core





## [0.8.77](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.76...@vtj/core@0.8.77) (2024-07-09)

**Note:** Version bump only for package @vtj/core





## [0.8.76](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.75...@vtj/core@0.8.76) (2024-07-08)

**Note:** Version bump only for package @vtj/core





## [0.8.75](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.74...@vtj/core@0.8.75) (2024-07-08)


### Features

* ✨ project dsl 删除版本号标识，减少冲突 ([69149c2](https://gitee.com/newgateway/vtj/commits/69149c21d50dc7673c880910ce960124550bf864))





## [0.8.74](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.73...@vtj/core@0.8.74) (2024-07-06)

**Note:** Version bump only for package @vtj/core





## [0.8.73](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.72...@vtj/core@0.8.73) (2024-07-06)

**Note:** Version bump only for package @vtj/core





## [0.8.72](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.71...@vtj/core@0.8.72) (2024-07-06)


### Features

* ✨ 支持项目部署二级目录配置 ([200e87b](https://gitee.com/newgateway/vtj/commits/200e87bea6ed66776d367164e2c84584e3cd22a1))





## [0.8.71](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.70...@vtj/core@0.8.71) (2024-07-05)

**Note:** Version bump only for package @vtj/core





## [0.8.70](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.69...@vtj/core@0.8.70) (2024-07-05)

**Note:** Version bump only for package @vtj/core





## [0.8.69](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.68...@vtj/core@0.8.69) (2024-07-04)

**Note:** Version bump only for package @vtj/core





## [0.8.68](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.67...@vtj/core@0.8.68) (2024-07-04)

**Note:** Version bump only for package @vtj/core





## [0.8.67](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.66...@vtj/core@0.8.67) (2024-07-03)

**Note:** Version bump only for package @vtj/core





## [0.8.66](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.65...@vtj/core@0.8.66) (2024-07-03)

**Note:** Version bump only for package @vtj/core





## [0.8.65](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.64...@vtj/core@0.8.65) (2024-07-02)

**Note:** Version bump only for package @vtj/core





## [0.8.64](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.63...@vtj/core@0.8.64) (2024-06-28)

**Note:** Version bump only for package @vtj/core





## [0.8.63](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.62...@vtj/core@0.8.63) (2024-06-28)

**Note:** Version bump only for package @vtj/core





## [0.8.62](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.61...@vtj/core@0.8.62) (2024-06-27)

**Note:** Version bump only for package @vtj/core





## [0.8.61](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.60...@vtj/core@0.8.61) (2024-06-26)

**Note:** Version bump only for package @vtj/core





## [0.8.60](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.59...@vtj/core@0.8.60) (2024-06-26)

**Note:** Version bump only for package @vtj/core





## [0.8.59](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.58...@vtj/core@0.8.59) (2024-06-26)

**Note:** Version bump only for package @vtj/core





## [0.8.58](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.57...@vtj/core@0.8.58) (2024-06-26)

**Note:** Version bump only for package @vtj/core





## [0.8.57](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.56...@vtj/core@0.8.57) (2024-06-25)

**Note:** Version bump only for package @vtj/core





## [0.8.56](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.55...@vtj/core@0.8.56) (2024-06-22)

**Note:** Version bump only for package @vtj/core





## [0.8.55](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.54...@vtj/core@0.8.55) (2024-06-21)

**Note:** Version bump only for package @vtj/core





## [0.8.54](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.53...@vtj/core@0.8.54) (2024-06-20)

**Note:** Version bump only for package @vtj/core





## [0.8.53](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.52...@vtj/core@0.8.53) (2024-06-20)

**Note:** Version bump only for package @vtj/core





## [0.8.52](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.51...@vtj/core@0.8.52) (2024-06-18)

**Note:** Version bump only for package @vtj/core





## [0.8.51](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.50...@vtj/core@0.8.51) (2024-06-16)

**Note:** Version bump only for package @vtj/core





## [0.8.50](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.49...@vtj/core@0.8.50) (2024-06-14)

**Note:** Version bump only for package @vtj/core





## [0.8.49](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.48...@vtj/core@0.8.49) (2024-06-14)

**Note:** Version bump only for package @vtj/core





## [0.8.48](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.47...@vtj/core@0.8.48) (2024-06-14)

**Note:** Version bump only for package @vtj/core





## [0.8.47](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.46...@vtj/core@0.8.47) (2024-06-14)

**Note:** Version bump only for package @vtj/core





## [0.8.46](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.45...@vtj/core@0.8.46) (2024-06-14)

**Note:** Version bump only for package @vtj/core





## [0.8.45](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.44...@vtj/core@0.8.45) (2024-06-14)

**Note:** Version bump only for package @vtj/core





## [0.8.44](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.43...@vtj/core@0.8.44) (2024-06-13)

**Note:** Version bump only for package @vtj/core





## [0.8.43](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.42...@vtj/core@0.8.43) (2024-06-12)

**Note:** Version bump only for package @vtj/core





## [0.8.42](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.41...@vtj/core@0.8.42) (2024-06-12)

**Note:** Version bump only for package @vtj/core





## [0.8.41](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.40...@vtj/core@0.8.41) (2024-06-12)

**Note:** Version bump only for package @vtj/core





## [0.8.40](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.39...@vtj/core@0.8.40) (2024-06-06)

**Note:** Version bump only for package @vtj/core





## [0.8.39](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.38...@vtj/core@0.8.39) (2024-05-30)


### Features

* ✨ 事件插槽定义支持配置参数 ([d6728b3](https://gitee.com/newgateway/vtj/commits/d6728b330815a937cb582f0d0afb667f54bc590e))





## [0.8.38](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.37...@vtj/core@0.8.38) (2024-05-29)

**Note:** Version bump only for package @vtj/core





## [0.8.37](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.36...@vtj/core@0.8.37) (2024-05-28)

**Note:** Version bump only for package @vtj/core





## [0.8.36](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.35...@vtj/core@0.8.36) (2024-05-27)

**Note:** Version bump only for package @vtj/core





## [0.8.35](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.34...@vtj/core@0.8.35) (2024-05-27)

**Note:** Version bump only for package @vtj/core





## [0.8.34](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.33...@vtj/core@0.8.34) (2024-05-24)

**Note:** Version bump only for package @vtj/core





## [0.8.33](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.32...@vtj/core@0.8.33) (2024-05-23)

**Note:** Version bump only for package @vtj/core





## [0.8.32](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.30...@vtj/core@0.8.32) (2024-05-23)

**Note:** Version bump only for package @vtj/core





## [0.8.31](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.30...@vtj/core@0.8.31) (2024-05-14)

**Note:** Version bump only for package @vtj/core





## [0.8.30](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.29...@vtj/core@0.8.30) (2024-05-11)

**Note:** Version bump only for package @vtj/core





## [0.8.29](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.28...@vtj/core@0.8.29) (2024-05-11)

**Note:** Version bump only for package @vtj/core





## [0.8.28](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.27...@vtj/core@0.8.28) (2024-05-09)

**Note:** Version bump only for package @vtj/core





## [0.8.27](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.26...@vtj/core@0.8.27) (2024-05-08)


### Features

* ✨ 支持metaQuery ([94c2879](https://gitee.com/newgateway/vtj/commits/94c287930d3ae9bafab7419673b4ec5c4fc5c73a))





## [0.8.26](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.25...@vtj/core@0.8.26) (2024-05-07)

**Note:** Version bump only for package @vtj/core





## [0.8.25](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.24...@vtj/core@0.8.25) (2024-05-07)

**Note:** Version bump only for package @vtj/core





## [0.8.24](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.23...@vtj/core@0.8.24) (2024-05-07)

**Note:** Version bump only for package @vtj/core





## [0.8.23](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.22...@vtj/core@0.8.23) (2024-05-07)

**Note:** Version bump only for package @vtj/core





## [0.8.22](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.21...@vtj/core@0.8.22) (2024-05-07)

**Note:** Version bump only for package @vtj/core





## [0.8.21](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.20...@vtj/core@0.8.21) (2024-05-06)

**Note:** Version bump only for package @vtj/core





## [0.8.20](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.19...@vtj/core@0.8.20) (2024-05-06)

**Note:** Version bump only for package @vtj/core





## [0.8.19](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.18...@vtj/core@0.8.19) (2024-05-06)


### Features

* ✨ auto plugins from package.json ([f327858](https://gitee.com/newgateway/vtj/commits/f3278585be56c841b672745bba5be780f26fb054))





## [0.8.18](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.17...@vtj/core@0.8.18) (2024-05-04)

**Note:** Version bump only for package @vtj/core





## [0.8.17](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.16...@vtj/core@0.8.17) (2024-05-02)

**Note:** Version bump only for package @vtj/core





## [0.8.16](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.15...@vtj/core@0.8.16) (2024-05-02)

**Note:** Version bump only for package @vtj/core





## [0.8.15](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.14...@vtj/core@0.8.15) (2024-05-02)

**Note:** Version bump only for package @vtj/core





## [0.8.14](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.13...@vtj/core@0.8.14) (2024-05-02)

**Note:** Version bump only for package @vtj/core





## [0.8.13](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.12...@vtj/core@0.8.13) (2024-05-02)


### Bug Fixes

* 🐛 偶发获取本地文件失败 ([c57e9d7](https://gitee.com/newgateway/vtj/commits/c57e9d766f40949da74d5eb24e94e84f83836a91))





## [0.8.12](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.11...@vtj/core@0.8.12) (2024-05-01)

**Note:** Version bump only for package @vtj/core





## [0.8.11](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.10...@vtj/core@0.8.11) (2024-05-01)

**Note:** Version bump only for package @vtj/core





## [0.8.10](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.9...@vtj/core@0.8.10) (2024-05-01)

**Note:** Version bump only for package @vtj/core





## [0.8.9](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.8...@vtj/core@0.8.9) (2024-04-30)


### Features

* ✨ 设计器支持远程扩展 ([ed2ed8e](https://gitee.com/newgateway/vtj/commits/ed2ed8ec38f51d389d0eb05488a3e8e06a1fdd35))






## [0.8.8](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.7...@vtj/core@0.8.8) (2024-04-27)

**Note:** Version bump only for package @vtj/core





## [0.8.7](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.5...@vtj/core@0.8.7) (2024-04-26)


### Features

* ✨ add charts module ([2e5b7e9](https://gitee.com/newgateway/vtj/commits/2e5b7e9ca763a2446d3e65af6fa8d1d32b8f2243))





## [0.8.6](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.5...@vtj/core@0.8.6) (2024-04-26)


### Features

* ✨ add charts module ([2e5b7e9](https://gitee.com/newgateway/vtj/commits/2e5b7e9ca763a2446d3e65af6fa8d1d32b8f2243))






## [0.8.5](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.4...@vtj/core@0.8.5) (2024-04-24)

**Note:** Version bump only for package @vtj/core





## [0.8.4](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.3...@vtj/core@0.8.4) (2024-04-24)

**Note:** Version bump only for package @vtj/core






## [0.8.3](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.2...@vtj/core@0.8.3) (2024-04-23)

**Note:** Version bump only for package @vtj/core






## [0.8.2](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.1...@vtj/core@0.8.2) (2024-04-22)

**Note:** Version bump only for package @vtj/core





## [0.8.1](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.8.0...@vtj/core@0.8.1) (2024-04-22)


### Bug Fixes

* 🐛 cli template ([911c3a0](https://gitee.com/newgateway/vtj/commits/911c3a0e2bb60548affe5dcf5a496577809d63b8))






# [0.8.0](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.7.34...@vtj/core@0.8.0) (2024-04-22)


### Features

* ✨ 区块协议支持来源 ([0b0405e](https://gitee.com/newgateway/vtj/commits/0b0405ed98f35add403d155022f6998a3ced16f3))
* ✨ 依赖支持语言包设置 ([362190e](https://gitee.com/newgateway/vtj/commits/362190e66d663412d2d07261bc29b9ef439af8ed))
* ✨ 支持 UrlSchema ([edae2c5](https://gitee.com/newgateway/vtj/commits/edae2c52ce88ad72a4a7c31844385dc083249e72))
* ✨ 支持区块插件 ([88b5028](https://gitee.com/newgateway/vtj/commits/88b5028cdb142dd9f5642c51ecb9f978333858ce))
* ✨ add FileSetter ([8467349](https://gitee.com/newgateway/vtj/commits/8467349162e8694808fd3fdcd2354d8bb6de086f))
* ✨ base service 支持 static files ([8875c7c](https://gitee.com/newgateway/vtj/commits/8875c7c4e7d14ccbe2dee89d8a214d40a4c57546))
* ✨ cli 支持 插件项目 ([f8a61c9](https://gitee.com/newgateway/vtj/commits/f8a61c97f7e94a6a4afd23e91c6d2b879cf8eaa3))
* ✨ x-attachment ([09a3391](https://gitee.com/newgateway/vtj/commits/09a33914ee22a2410a396ed004a799d368259987))


### Performance Improvements

* ⚡ 代码优化 ([8d5e2d3](https://gitee.com/newgateway/vtj/commits/8d5e2d366876cd1c91eb8e4c7b30237e65dd33b5))






## [0.7.34](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.7.33...@vtj/core@0.7.34) (2024-04-10)

**Note:** Version bump only for package @vtj/core






## [0.7.33](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.7.32...@vtj/core@0.7.33) (2024-04-08)

**Note:** Version bump only for package @vtj/core






## [0.7.32](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.7.31...@vtj/core@0.7.32) (2024-04-03)

**Note:** Version bump only for package @vtj/core






## [0.7.31](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.7.30...@vtj/core@0.7.31) (2024-04-03)


### chore

* 🚀 格式化提交信息 ([fede392](https://gitee.com/newgateway/vtj/commits/fede3924392a8297d2b2fe37565fd975116b8bf2))


### Features

* ✨ api mock ([df7400f](https://gitee.com/newgateway/vtj/commits/df7400f1c2f7aa20f24e5217b177a38877de5cdd))


### BREAKING CHANGES

* 🧨 no






## [0.7.31](https://gitee.com/newgateway/vtj/compare/@vtj/core@0.7.30...@vtj/core@0.7.31) (2024-04-03)


### chore

* 🚀 格式化提交信息 ([fede392](https://gitee.com/newgateway/vtj/commits/fede3924392a8297d2b2fe37565fd975116b8bf2))


### Features

* ✨ api mock ([df7400f](https://gitee.com/newgateway/vtj/commits/df7400f1c2f7aa20f24e5217b177a38877de5cdd))


### BREAKING CHANGES

* 🧨 no
